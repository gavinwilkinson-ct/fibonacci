module Main where

fibonacci :: Int -> [Int]
fibonacci x = fibonacci' x [1,0]

nextFibonacci :: [Int]->[Int]
nextFibonacci (a:cs) = a+head cs:a:cs

fibonacci' :: Int -> [Int]->[Int]
fibonacci' y (x:xs)   | length (x:xs) == y = x:xs
                            | otherwise = fibonacci' y (nextFibonacci (x:xs))

main :: IO ()
main = do
    putStrLn $ show $ fibonacci 50
